package tp2.punto4b;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;

import javax.imageio.ImageIO;

import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;
import com.rabbitmq.client.DeliverCallback;
import org.json.JSONObject;
import java.awt.image.BufferedImage;

import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;
import com.rabbitmq.client.MessageProperties;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.apache.tomcat.util.codec.binary.Base64;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.multipart.MultipartFile;

import javax.imageio.ImageIO;


import java.awt.image.BufferedImage;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;
import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.*;
import java.awt.*;
import org.json.JSONObject;
import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.*;

public class SobelWorker {
    private String ipRMQ = "localhost";
    private int portRMQ = 5672;
    private String userRMQ = "guest";
    private String passRMQ = "guest";
    private String vHost = "/";
    private final static String QUEUE_PENDINGS_NAME = "Tareas_Pendientes";
    private final static String QUEUE_PROCESED_NAME = "Tareas_Procesadas";

    private ConnectionFactory conexRMQFactory;
    private Connection conexRMQ;
    private Channel channel;

    Logger logger = LoggerFactory.getLogger(SobelWorker.class);

    public SobelWorker() {
        this.conexRMQFactory = new ConnectionFactory();
        this.conexRMQFactory.setHost(ipRMQ);
        this.conexRMQFactory.setPort(portRMQ);
        this.conexRMQFactory.setUsername(userRMQ);
        this.conexRMQFactory.setPassword(passRMQ);
        this.conexRMQFactory.setVirtualHost(vHost);

        try {
            this.conexRMQ = conexRMQFactory.newConnection();
            this.channel = this.conexRMQ.createChannel();
            this.channel.queueDeclare(QUEUE_PENDINGS_NAME, true, false, false, null);
            logger.info(" [*] Esperando por tareas. Presione CTRL+C para salir.");

            DeliverCallback deliverCallback = (consumerTag, delivery) -> {
                String mensaje = new String(delivery.getBody(), "UTF-8");
                logger.info("[*] Se recibio una tarea: ");
                try {
                    logger.info("[*]Procesando la tarea recibida.");
                    doWork(mensaje);
                    logger.info("[*]Tarea Procesada. Enviando ACK");
                    channel.basicAck(delivery.getEnvelope().getDeliveryTag(), false);
                    logger.info("[*] ACK enviado.");
                } catch (Exception e) {
                    logger.error("[!]Error al Procesar la Subtarea: " + e.getMessage());
                }
            };
            boolean autoAck = false; // Ahora se debe mandar un ack para que se borre el mensaje de la cola.
            channel.basicConsume(QUEUE_PENDINGS_NAME, autoAck, deliverCallback, consumerTag -> {
            });

        } catch (Exception e) {
            logger.error("[!]Error al crear Conexion con la Cola de Subtareas Pendientes: " + e.getMessage());
        }finally{
            
        }
    }

    private void doWork(String task) throws InterruptedException, IOException {
        JSONObject objetoJson = new JSONObject(task);
        byte[] payloadProcesado = filtroSobel(objetoJson);

        // convertimos la imagen a base64 para su transferencia en json
        JSONObject obj = new JSONObject();
        obj.put("idSubtarea", objetoJson.get("idSubtarea"));
        obj.put("idTarea", objetoJson.get("idTarea"));
        obj.put("nroOrden", objetoJson.get("nroOrden"));
        obj.put("payload", Base64.encodeBase64String(payloadProcesado));

        //Encolar a Cola de Procesados
        logger.info("Enviando imagen...");
        this.channel.queueDeclare(QUEUE_PROCESED_NAME, true, false, false, null);
        this.channel.basicPublish("", QUEUE_PROCESED_NAME, null, obj.toString().getBytes());

    }

    private byte[] filtroSobel(JSONObject objJson) throws InterruptedException, IOException {

        // Aplicamos el algoritmo sobel a la imagen
        logger.info("id de la sub tarea a procesar "+ String.valueOf(objJson.get("idSubtarea")));
        // obtenemos la parte de la imagen que corresponde a este worker
        
        byte[] bytesFromDecode = Base64.decodeBase64(objJson.getString("payload"));

        InputStream is = new ByteArrayInputStream(bytesFromDecode);
        BufferedImage image = ImageIO.read(is);
        // try {
        //     ImageIO.write(image, "jpg", new File("pruebita sin transformar"+".jpg"));
        // } catch (IOException e) {
        //     TODO Auto-generated catch block
        //     e.printStackTrace();
        // }

        int x = image.getWidth();
        int y = image.getHeight();

        int maxGval = 0;
        int[][] edgeColors = new int[x][y];
        int maxGradient = -1;

        logger.info("Iniciando conversion de imagen ");
        for (int i = 1; i < x - 1; i++) {
            for (int j = 1; j < y - 1; j++) {

                int val00 = getGrayScale(image.getRGB(i - 1, j - 1));
                int val01 = getGrayScale(image.getRGB(i - 1, j));
                int val02 = getGrayScale(image.getRGB(i - 1, j + 1));

                int val10 = getGrayScale(image.getRGB(i, j - 1));
                int val11 = getGrayScale(image.getRGB(i, j));
                int val12 = getGrayScale(image.getRGB(i, j + 1));

                int val20 = getGrayScale(image.getRGB(i + 1, j - 1));
                int val21 = getGrayScale(image.getRGB(i + 1, j));
                int val22 = getGrayScale(image.getRGB(i + 1, j + 1));

                int gx = ((-1 * val00) + (0 * val01) + (1 * val02))
                        + ((-2 * val10) + (0 * val11) + (2 * val12))
                        + ((-1 * val20) + (0 * val21) + (1 * val22));

                int gy = ((-1 * val00) + (-2 * val01) + (-1 * val02))
                        + ((0 * val10) + (0 * val11) + (0 * val12))
                        + ((1 * val20) + (2 * val21) + (1 * val22));

                double gval = Math.sqrt((gx * gx) + (gy * gy));
                int g = (int) gval;

                if (maxGradient < g) {
                    maxGradient = g;
                }

                edgeColors[i][j] = g;
            }
        }

        double scale = 255.0 / maxGradient;

        for (int i = 1; i < x - 1; i++) {
            for (int j = 1; j < y - 1; j++) {
                int edgeColor = edgeColors[i][j];
                edgeColor = (int) (edgeColor * scale);
                edgeColor = 0xff000000 | (edgeColor << 16) | (edgeColor << 8) | edgeColor;

                image.setRGB(i, j, edgeColor);
            }
        }

        //TODO debugeando
        // try {
        //     ImageIO.write(image, "jpg", new File("pruebita"+".jpg"));
        // } catch (IOException e) {
        //     // TODO Auto-generated catch block
        //     e.printStackTrace();
        // }

        // BufferedUImage to byte[]
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        ImageIO.write(image, "jpg", baos);
        byte[] bytes = baos.toByteArray();
        return bytes;
    }

    public static int getGrayScale(int rgb) {
        int r = (rgb >> 16) & 0xff;
        int g = (rgb >> 8) & 0xff;
        int b = (rgb) & 0xff;

        // from https://en.wikipedia.org/wiki/Grayscale, calculating luminance
        int gray = (int) (0.2126 * r + 0.7152 * g + 0.0722 * b);
        // int gray = (r + g + b) / 3;
        return gray;
    }
    public static void main(String[] args) {
        SobelWorker sobelWorker = new SobelWorker();
    }

}