package tp2.punto4.server_maestro.database.entidades;

import com.sun.istack.NotNull;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

@Entity
@Table(name="tareas")
public class Tarea {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int idtarea;
    @NotNull
    private int cantidadPartes;
    @NotNull
    private String nombreArchivo;
    @NotNull
    private String estado;

    @OneToMany(mappedBy = "tarea",cascade = CascadeType.ALL) // El borrado de la tarea borrara tambien los procesos asociados
    private Set<Subtarea> subtareas = new HashSet<>();

    public int getIdtarea() {
        return idtarea;
    }

    public void setIdtarea(int idtarea) {
        this.idtarea = idtarea;
    }

    public int getCantidadPartes() {
        return cantidadPartes;
    }

    public void setCantidadPartes(int cantidadPartes) {
        this.cantidadPartes = cantidadPartes;
    }

    public String getNombreArchivo() {
        return nombreArchivo;
    }

    public void setNombreArchivo(String nombreArchivo) {
        this.nombreArchivo = nombreArchivo;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    public Set<Subtarea> getSubtareas() {
        return subtareas;
    }

    public void setSubtareas(Set<Subtarea> subtareas) {
        this.subtareas = subtareas;

        //Le asignamos la tarea a cada subtarea.
        for (Subtarea subtarea: subtareas
             ) {
            subtarea.setTarea(this);
        }
    }

    public void addSubtarea(Subtarea tarea){
        this.subtareas.add(tarea);
    }
}
