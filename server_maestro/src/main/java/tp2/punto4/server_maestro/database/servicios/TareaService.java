package tp2.punto4.server_maestro.database.servicios;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import tp2.punto4.server_maestro.database.entidades.Tarea;
import tp2.punto4.server_maestro.database.repositorios.TareaRepository;

@Service
public class TareaService {
    @Autowired
    private TareaRepository repository;

    public void saveTarea(Tarea tarea){
        repository.save(tarea);
    }

    public Tarea getTareaById(int idtarea){
        return repository.findById(idtarea).orElse(null);
    }
}
