package tp2.punto4.server_maestro.database.repositorios;

import org.springframework.data.jpa.repository.JpaRepository;
import tp2.punto4.server_maestro.database.entidades.Tarea;

public interface TareaRepository extends JpaRepository<Tarea,Integer> {

}
