package tp2.punto4.server_maestro.database.servicios;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import tp2.punto4.server_maestro.database.entidades.Subtarea;
import tp2.punto4.server_maestro.database.repositorios.SubtareaRepository;


@Service
public class SubtareaService {
    @Autowired
    private SubtareaRepository repository;

    public void saveSubtarea(Subtarea subtarea){
        repository.save(subtarea);
    }

    public Subtarea getSubtareaById(int idsubtarea){
        return repository.findById(idsubtarea).orElse(null);
    }
}
