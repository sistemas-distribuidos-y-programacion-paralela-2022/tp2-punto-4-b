package tp2.punto4.server_maestro.database.repositorios;

import org.springframework.data.jpa.repository.JpaRepository;
import tp2.punto4.server_maestro.database.entidades.Subtarea;

public interface SubtareaRepository extends JpaRepository<Subtarea,Integer> {
}
