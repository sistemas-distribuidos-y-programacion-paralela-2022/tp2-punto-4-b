package tp2.punto4.server_maestro.database.entidades;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.sun.istack.NotNull;
import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

import javax.persistence.*;

@Entity
@Table(name="subtareas")
public class Subtarea {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY) // le decimos que el id sea autogenerado
    private int idsubtarea;

    @ManyToOne(fetch = FetchType.LAZY,optional = false)// Lazy porque solo queremos que nos de un dato cuando le indiquemos
    @JoinColumn(name = "idtarea")
    @JsonProperty(access = JsonProperty.Access.WRITE_ONLY) // Para que no hayan problemas de serializacion del objeto tarea. Evitamos que se serialice
    @Fetch(FetchMode.JOIN)
    private Tarea tarea;
    @NotNull
    private String estado;

    private int nro_orden;

    public int getIdsubtarea() {
        return idsubtarea;
    }

    public void setIdsubtarea(int idsubtarea) {
        this.idsubtarea = idsubtarea;
    }

    public Tarea getTarea() {
        return tarea;
    }

    public void setTarea(Tarea tarea) {
        this.tarea = tarea;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    public int getNro_orden() {
        return nro_orden;
    }

    public void setNro_orden(int nro_orden) {
        this.nro_orden = nro_orden;
    }

    @Override
    public String toString() {
        return "Subtarea{" +
                "idsubtarea=" + idsubtarea +
                ", tarea=" + tarea +
                ", estado='" + estado + '\'' +
                ", nro_orden=" + nro_orden +
                '}';
    }
}
