package tp2.punto4.server_maestro.controllers;

import com.rabbitmq.client.*;

import org.json.JSONObject;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.RestController;
import tp2.punto4.server_maestro.database.entidades.Subtarea;
import tp2.punto4.server_maestro.database.entidades.Tarea;
import tp2.punto4.server_maestro.database.repositorios.SubtareaRepository;
import tp2.punto4.server_maestro.database.repositorios.TareaRepository;
import tp2.punto4.server_maestro.database.servicios.SubtareaService;
import tp2.punto4.server_maestro.database.servicios.TareaService;

import java.util.*;
import java.io.*;
import java.awt.image.BufferedImage;
import javax.imageio.ImageIO;
import org.apache.tomcat.util.codec.binary.Base64;

public class QueueController {
    private final static String QUEUE_PROCESED_NAME = "Tareas_Procesadas";// Cambio temporal
    private String ipRMQ = "localhost";
    private int portRMQ = 5672;
    private String userRMQ = "guest";
    private String passRMQ = "guest";
    private String vHost = "/";

    Logger logger = LoggerFactory.getLogger(QueueController.class);

    private ConnectionFactory conexRMQFactory;
    private Connection conexRMQ;
    private Channel channel;
    // Manejo de la BD
    private TareaService tareaSrv;

    private SubtareaService subtareaSrv;

    private List<Imagen> imagenes;

    public QueueController(List<Imagen> imagenes) {
        this.conexRMQFactory = new ConnectionFactory();
        this.conexRMQFactory.setHost(ipRMQ);
        this.conexRMQFactory.setPort(portRMQ);
        this.conexRMQFactory.setUsername(userRMQ);
        this.conexRMQFactory.setPassword(passRMQ);
        this.conexRMQFactory.setVirtualHost(vHost);
        this.imagenes = imagenes;

        try {
            this.conexRMQ = conexRMQFactory.newConnection();
            this.channel = conexRMQ.createChannel();
            channel.queueDeclare(QUEUE_PROCESED_NAME, true, false, false, null);
            logger.info(" [*] Esperando por Subtareas. Presione CTRL+C para salir.");

            DeliverCallback deliverCallback = (consumerTag, delivery) -> {
                String mensaje = new String(delivery.getBody(), "UTF-8");
                try {
                    manejarCola(mensaje, this.subtareaSrv);
                    logger.info("[*]Tarea Procesada. Enviando ACK");
                    channel.basicAck(delivery.getEnvelope().getDeliveryTag(), false);
                } catch (Exception e) {
                    logger.error("[!]Error al Procesar la Subtarea: " + e.getMessage());
                }
            };
            boolean autoAck = false; // Ahora se debe mandar un ack para que se borre el mensaje de la cola.
            channel.basicConsume(QUEUE_PROCESED_NAME, autoAck, deliverCallback, consumerTag -> {
            });

        } catch (Exception e) {
            logger.error("[!]Error al crear Conexion con la Cola de Subtareas Pendientes: " + e.getMessage());
        } finally {

        }
    }

    public void setRepositories(TareaService tareaSrv, SubtareaService subtareaSrv) {
        this.tareaSrv = tareaSrv;
        this.subtareaSrv = subtareaSrv;
    }

    private void manejarCola(String task, SubtareaService subtareasrv) throws InterruptedException, IOException {
        JSONObject objetoJson = new JSONObject(task);
        logger.info("[*] Se recibio la Subtarea[" + objetoJson.getInt("idSubtarea") + "] perteneciente a la Tarea["
                + objetoJson.getInt("idTarea") + "]");
        logger.info("[*]Procesando la Subtarea recibida.");
        Imagen imagen;
        Subtarea st = subtareasrv.getSubtareaById(objetoJson.getInt("idSubtarea"));

        // TODO Consultar si la Tarea padre ya existe en el array "imagenes"
        int i = 0;
        // if (this.imagenes.size() == 0) {
        //     imagen = new Imagen(objetoJson.getInt("idTarea"), String.valueOf(objetoJson.getInt("idTarea")));
        //     imagen.addSubtarea(objetoJson);
        //     this.imagenes.add(imagen);
        // } else {
        //     imagen = this.imagenes.get(i);
        //     imagen.addSubtarea(objetoJson);
        // }

        int j = getPosicionImagen(this.imagenes, objetoJson.getInt("idTarea"));
        if (j == -1) {
            imagen = new Imagen(objetoJson.getInt("idTarea"), String.valueOf(objetoJson.getInt("idTarea")));
            imagen.addSubtarea(objetoJson);
            this.imagenes.add(imagen);
            j=0;
        } else {
            // actualizo la subtarea de la tarea (Imagen)
            imagen = this.imagenes.get(j);
            imagen.addSubtarea(objetoJson);
            this.imagenes.set(j, imagen);

        }
        for (JSONObject subtask : imagen.getSubtareasJson()) {
            logger.info("Sub imagen: " + subtask.get("idSubtarea"));
            i++;
        }
        // TODO Hacer dinamico la cantidad de subtareas
        if (i == 16) {
            logger.info("Se encontraron las 16 subtareas");
            Imagen imagenOrdenada = this.imagenes.get(j);
            imagenOrdenada.sortSubtareas();
            this.imagenes.set(j, imagenOrdenada);
            mergeImage(this.imagenes.get(j));
            Tarea tarea = tareaSrv.getTareaById(objetoJson.getInt("idTarea"));
            tarea.setEstado("Procesada");
            tareaSrv.saveTarea(tarea);
        }
        // Actualiza el estado de la Subtarea a "Procesada".
        actualizarEstadoSubtareaPendiente(objetoJson.getInt("idSubtarea"));

    }

    private int getPosicionImagen(List<Imagen> imagenes, int idTarea) {
        int i = 0;
        while (i < imagenes.size()) {
            if (imagenes.get(i).getIdTarea() == idTarea) {
                return i;
            }
            i++;
        }
        return -1;
    }

    private void actualizarEstadoSubtareaPendiente(int idSubtarea) {
        try {
            logger.info("EL id que viene: " + idSubtarea);
            // Actualizamos las subtareas de la bd.
            Subtarea st = this.subtareaSrv.getSubtareaById(idSubtarea);
            logger.info(st.toString());
            st.setEstado("Procesada");
            this.subtareaSrv.saveSubtarea(st);

        } catch (Exception e) {
            logger.error("[!]Error al actualizar Estado de Subtarea de la BD: " + e.getMessage());
            e.printStackTrace();
        }
    }

    private void mergeImage(Imagen imagen) {
        BufferedImage[] buffImages2 = new BufferedImage[16];

        for (int i = 0; i < imagen.getSubtareasJson().size(); i++) {
            try {
                byte[] bytesFromDecode = Base64.decodeBase64(imagen.getSubtareasJson().get(i).getString("payload"));
                InputStream is = new ByteArrayInputStream(bytesFromDecode);
                BufferedImage image = ImageIO.read(is);
                buffImages2[i] = image;

            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        }
        int rows = 4;
        int cols = 4;
        int chunks = rows * cols;

        int chunkWidth, chunkHeight;
        int type;

        type = buffImages2[0].getType();
        chunkWidth = buffImages2[0].getWidth();
        chunkHeight = buffImages2[0].getHeight();

        // Initializing the final image
        BufferedImage finalImg = new BufferedImage(chunkWidth * cols, chunkHeight * rows, type);

        int num = 0;
        for (int i = 0; i < rows; i++) {
            for (int j = 0; j < cols; j++) {
                finalImg.createGraphics().drawImage(buffImages2[num], chunkWidth * j, chunkHeight * i, null);
                num++;
            }
        }
        try {
            ImageIO.write(finalImg, "jpeg", new File("server_maestro/src/main/resources/" + imagen.getName() + ".jpg"));
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

    }
}
