package tp2.punto4.server_maestro.controllers;

import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;

import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.apache.commons.io.IOUtils;
import org.apache.tomcat.util.codec.binary.Base64;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.multipart.MultipartFile;
import tp2.punto4.server_maestro.database.entidades.Subtarea;
import tp2.punto4.server_maestro.database.entidades.Tarea;
import tp2.punto4.server_maestro.database.servicios.SubtareaService;
import tp2.punto4.server_maestro.database.servicios.TareaService;

import java.awt.image.BufferedImage;

import javax.annotation.PostConstruct;
import javax.imageio.ImageIO;
import java.io.*;
import java.awt.*;
import java.util.ArrayList;
import java.util.List;

@RestController
public class ServerMaestro {
    private final static String QUEUE_NAME = "Tareas_Pendientes";
    private String ipRMQ = "localhost";
    private int portRMQ = 5672;
    private String userRMQ = "guest";
    private String passRMQ = "guest";
    private String vHost = "/";

    Logger logger = LoggerFactory.getLogger(ServerMaestro.class);

    private ConnectionFactory conexRMQFactory;
    private Connection conexRMQ;
    private Channel channelRMQ;

    // Manejo de la BD
    @Autowired
    private TareaService tareaSrv;
    @Autowired
    private SubtareaService subtareaSrv;
    private QueueController en;


    public ServerMaestro() {
        // Creando Factory de Conexion
        logger.info("Abriendo conexion con servidor de Colas");
        this.conexRMQFactory = new ConnectionFactory();
        this.conexRMQFactory.setHost(ipRMQ);
        this.conexRMQFactory.setPort(portRMQ);
        this.conexRMQFactory.setUsername(userRMQ);
        this.conexRMQFactory.setPassword(passRMQ);
        this.conexRMQFactory.setVirtualHost(vHost);

        // Creando Conexion con Servicio de Cola
        try {
            this.conexRMQ = this.conexRMQFactory.newConnection();
            this.channelRMQ = this.conexRMQ.createChannel();
            this.channelRMQ.queueDeclare(QUEUE_NAME, true, false, false, null);
            logger.info("Se crea conexion al Servidor de Cola [ip: " + ipRMQ + "port: " + portRMQ + "]");

            this.channelRMQ.basicQos(1);
            logger.info("Se declara que RabbitMQ no despache mensajes a los workers si no confirmaron el anterior");

        } catch (Exception e) {
            e.printStackTrace();
            logger.error("[!]Error al Crear la conexion con Servidor de Colas.");
        }

        // Se encargara de manejar la cola de procesados.
        List<Imagen> imagenes = new ArrayList<>();
        this.en = new QueueController(imagenes);
    }
    @PostConstruct
    public void init() {
        this.en.setRepositories(this.tareaSrv, this.subtareaSrv);
    }
    //ESTO NO SE VA A USAR///////////////////////////////////////////////

    @GetMapping(
            value ="/download/image",
            produces = MediaType.IMAGE_JPEG_VALUE) // I don't know why you guys comment and program in english, we are at unLu
    public byte[] downloadImage( @RequestParam(name = "idtarea") String idtarea ) throws IOException {

    //Localizar imagen en el filesystem y enviarselo.
    //mergearImagen();
        Tarea tarea = tareaSrv.getTareaById(Integer.valueOf(idtarea));

      if ( new File("server_maestro/src/main/resources/"+tarea.getIdtarea()+".jpg").isFile() ){
          InputStream in = getClass()
                  .getResourceAsStream("/"+tarea.getIdtarea()+".jpg");
          return IOUtils.toByteArray(in);
      }else{

          logger.error("[!]Error. Imagen no procesada aun.");
          return null;
      }

    //     TODO mmostrar la imagen transformada

    //     TODO logear

    //     TODO hacer un link desde la pagina de upload
    //     TODO agregar un id a la imagen
     }

    @PostMapping("/upload/image")
    public String uploadImage(@RequestParam("image") MultipartFile multipartFile)
            throws IOException {
        // Creamos la tarea en la bd.
        Tarea tarea = new Tarea();
        tarea.setNombreArchivo(multipartFile.getOriginalFilename());
        tarea.setEstado("Pendiente");
        this.tareaSrv.saveTarea(tarea);

        logger.info("Se recibio la imagen: " + multipartFile.getOriginalFilename());

        dividirImagen(multipartFile, tarea);
        return "<a href=http://localhost:9090/download/image/?idtarea="+tarea.getIdtarea()+">Descargar Imagen Final</a>";
    }

    private void dividirImagen(MultipartFile multipartFile, Tarea tarea) throws FileNotFoundException, IOException {
        // Codigo robado de
        // https://kalanir.blogspot.com/2010/02/how-to-split-image-into-chunks-java.html

        // convertimos el multiparFile en una bufferedImage
        byte[] byteArr = multipartFile.getBytes();
        InputStream inputStream = new ByteArrayInputStream(byteArr);
        BufferedImage image = ImageIO.read(inputStream); // reading the image file

        int rows = 4; // You should decide the values for rows and cols variables
        int cols = 4;
        int chunks = rows * cols;

        logger.info("Dividiendo la imagen en " + chunks + " pedazos");

        int chunkWidth = image.getWidth() / cols; // determines the chunk width and height
        int chunkHeight = image.getHeight() / rows;
        int count = 0;
        BufferedImage imgs[] = new BufferedImage[chunks]; // Image array to hold image chunks
        for (int x = 0; x < rows; x++) {
            for (int y = 0; y < cols; y++) {
                // Initialize the image array with image chunks
                imgs[count] = new BufferedImage(chunkWidth, chunkHeight, image.getType());
                // // Convertire el Objeto json y encolar los bytes del objeto.
                // draws the image chunk
                Graphics2D gr = imgs[count++].createGraphics();
                gr.drawImage(image, 0, 0, chunkWidth, chunkHeight, chunkWidth * y, chunkHeight * x,
                        chunkWidth * y + chunkWidth, chunkHeight * x + chunkHeight, null);
                gr.dispose();
                // una vez que terminamos de hacer la partecita
                // Convertir Objeto json a un byte[] y encolar.
            }
        }
        logger.info("Splitting done");

        // writing mini images into image files
        for (int i = 0; i < imgs.length; i++) {
            ImageIO.write(imgs[i], "jpg", new File(multipartFile.getOriginalFilename() + i + ".jpg"));

            // ida y vuelta conversion de imagen
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            ImageIO.write(imgs[i], "jpg", baos);
            byte[] bytes = baos.toByteArray();

            // Creamos subtarea en la bd
            Subtarea st = new Subtarea();
            st.setEstado("Pendiente");
            st.setNro_orden(i + 1);
            st.setTarea(tarea);
            this.subtareaSrv.saveSubtarea(st);

            // convertimos la imagen a base64 para su transferencia en json

            JSONObject obj = new JSONObject();
            obj.put("idSubtarea", st.getIdsubtarea());
            obj.put("idTarea", st.getTarea().getIdtarea());
            obj.put("nroOrden", st.getNro_orden());
            obj.put("payload", Base64.encodeBase64String(bytes));

            // Aca enviamos el pedacito de imagen

            this.channelRMQ.basicPublish("", QUEUE_NAME, null, obj.toString().getBytes());

            logger.info("parte " + (i + 1) + " encolada");
        }
        // Guardar Cantidad de partes total en la Tarea
        tarea.setCantidadPartes(count);
        this.tareaSrv.saveTarea(tarea);

        logger.info("Mini images created");
    }

}
