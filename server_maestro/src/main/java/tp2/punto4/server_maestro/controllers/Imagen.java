package tp2.punto4.server_maestro.controllers;

import java.util.ArrayList;
import java.util.List;
import org.json.JSONObject;

public class Imagen {
    private String name;
    private int idTarea;
    private List <JSONObject> subtareas;

    public int getIdTarea() {
        return this.idTarea;
    }

    public void setIdTarea(int idTarea) {
        this.idTarea = idTarea;
    }

    public Imagen(int idTarea, String name) {
        this.subtareas = new ArrayList<JSONObject>();
        this.name = name;
        this.idTarea = idTarea;
    }

    public String getName() {
        return this.name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<JSONObject> getSubtareasJson() {
        return this.subtareas;
    }

    public void setSubtareasJson(List<JSONObject> subtareas) {
        this.subtareas = subtareas;
    }
    public void addSubtarea(JSONObject objetoJson){
        this.subtareas.add(objetoJson);
    }
    public JSONObject getSubtarea(int nroOrden){
        return this.subtareas.get(nroOrden);
    }
    public void sortSubtareas(){

        List <JSONObject> subtareasOrdenadas = new ArrayList<JSONObject>() ;
        JSONObject object = new JSONObject();
        for (int j = 0; j < this.subtareas.size(); j++) {
            subtareasOrdenadas.add(object);  
        }
        for (int j = 0; j <  this.subtareas.size(); j++) {

            subtareasOrdenadas.set(this.subtareas.get(j).getInt("nroOrden")-1, this.subtareas.get(j));
        }
        this.setSubtareasJson(subtareasOrdenadas);
    }
    
}
