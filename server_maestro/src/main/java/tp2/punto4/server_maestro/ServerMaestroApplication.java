package tp2.punto4.server_maestro;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ServerMaestroApplication {

	public static void main(String[] args) {
		SpringApplication.run(ServerMaestroApplication.class, args);
	}

}
